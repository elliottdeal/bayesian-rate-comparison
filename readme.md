A Bayesian Approach to Comparing Two Rates
==========================================

This script is designed to compare two rates. This originated out of a comparison of conversion rates for A/B testing of two versions of a webpage but should be applicable to the comparison of any two rates. However, in an experimental sense, this comparison only makes sense when you control for any other factors beyond the "treatment" (being shown a different website in this example). In this example, you need to check that the assignment to version A or B is random, in the sense that the tests take place over the same periods in time, users land on the page from similar sources, have similar characteristics, and so forth

Even if the two groups seeing the different landing pages come from the same population, there is a chance the difference in sign-up rates results from random variation. Using a Bayesian approach, you can model the probability of the underlying "true" sign-up rates taking on any particular value. Grid approximation is one way to estimate the credibility of various underlying sign-up rates for versions A or B.



Technical Notes
---------------
This code uses only base R, and was written in version 3.5.0.


Todo
----
Add region of practical equivalence (ROPE), a la Kruschke.


References
-----------
The code is inspired by and partly adapted from Richard McElreath's "Statistical Rethinking". When I add the ROPE section, that will be inspired by Kruschke's "Bayesian estimation supersedes the t-test".
